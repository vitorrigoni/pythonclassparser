#!/usr/bin/python
# -*- coding: UTF-8 -*-

import re

class C:
	"""
	Parser do arquivo modelo para C
	"""

	def __init__(self, lines, file_name):
		self.data = lines
		
		header = open(file_name + ".h", "w+")
		header.write("#include <stdio.h>\n\n")

		self.file = open(file_name + ".c", "w+")
		self.file.write("#include <"+ file_name +".h>\n\n")

		self.comments = [i for i in self.data if i[0] == "#"]
		self.class_name = str([i for i in self.data if i[0] == "M"][0])
		self.fields = [i for i in self.data if i[0] == "F"]

		self.write_comments()
		self.write_fields()
		self.write_class()


	def write_comments(self):
		for line in self.comments:
			self.file.write("/* " + line[1:].strip() + " */\n")
		self.file.write("typedef struct {\n") #depois de escrever todos os comentários eu crio a definição de classe


	def write_class(self):
		self.file.write("} " + self.class_name[1:].strip() + ";\n")

	def write_fields(self):
		for i in self.fields:
			i = list(reversed(i[1:].strip().split("\t")))
			str_list = filter(None, i)

			match = re.search(r'(\[\d+\])', str_list[0])
			if match:
				size = match.group(0)
				typeof = str_list[0].replace(size, "")
				self.file.write("\t{}\t{}{};\n".format(typeof, str_list[1], size))
			else:
				self.file.write("\t{}\t{};\n".format(str_list[0], str_list[1]))



		


























