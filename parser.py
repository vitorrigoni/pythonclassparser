#!/usr/bin/python
# -*- coding: UTF-8 -*-
'''
Parser para construção de classes em variadas Linguagens
Utilizado como ferramenta de estudo em python
@author Vitor Rigoni
'''

from classes.C import C

ling = {
	0: "C",
	1: "Pascal"
}

print("Linguagens disponíveis:\n")

for key, value in ling.iteritems():
	print("{}: {}".format(key, value))

selection = input("Selecione a linguagem que deseja gerar o arquivo pelo número:")

if selection == None or selection not in ling:
	print("Escolha uma linguagem dentre as disponíveis")
else:
	f = open("sample.txt", "r")

	lines = f.read().split("\n")
	if selection == 0:
		pars = C(lines, "sample")



